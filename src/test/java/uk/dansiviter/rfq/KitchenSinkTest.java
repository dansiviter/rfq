/*
 * Copyright 2020 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.dansiviter.rfq;

import static java.lang.String.format;
import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URI;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import javax.enterprise.inject.spi.CDI;
import javax.websocket.ClientEndpointConfig;
import javax.websocket.ContainerProvider;
import javax.websocket.DeploymentException;
import javax.websocket.EncodeException;
import javax.websocket.Endpoint;
import javax.websocket.EndpointConfig;
import javax.websocket.MessageHandler;
import javax.websocket.Session;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import io.helidon.microprofile.cdi.Main;
import io.helidon.microprofile.server.ServerCdiExtension;
import uk.dansiviter.rfq.api.BlotterMessage;
import uk.dansiviter.rfq.api.BookMessage;
import uk.dansiviter.rfq.api.CurrencyPair;
import uk.dansiviter.rfq.api.FilterMessage;
import uk.dansiviter.rfq.api.QuoteMessage;
import uk.dansiviter.rfq.api.RequestMessage;
import uk.dansiviter.rfq.api.RfqMessage;
import uk.dansiviter.rfq.entity.Trade.Status;
import uk.dansiviter.rfq.ws.BlotterMessageCodec;
import uk.dansiviter.rfq.ws.RfqMessageCodec;

public class KitchenSinkTest {

	private BlotterClient blotter;
	private RfqClient rfq;

	@BeforeAll
	public static void beforeAll() {
		Main.main(null);
	}

	@BeforeEach
	public void before() throws IOException, DeploymentException {
		var cdiExtension = CDI.current().getBeanManager().getExtension(ServerCdiExtension.class);
		var port = cdiExtension.port();
		var config = ClientEndpointConfig.Builder.create();
		var container = ContainerProvider.getWebSocketContainer();
		this.blotter = new BlotterClient();
		config.decoders(List.of(BlotterMessageCodec.class)).encoders(List.of(BlotterMessageCodec.class));
		container.connectToServer(this.blotter, config.build(),
				URI.create(format("ws://localhost:%d/ws/blotter/v1", port)));
		this.rfq = new RfqClient();
		config.decoders(List.of(RfqMessageCodec.class)).encoders(List.of(RfqMessageCodec.class));
		container.connectToServer(this.rfq, config.build(),
				URI.create(format("ws://localhost:%d/ws/rfq/v1/%s", port, UUID.randomUUID())));
	}

	@Test
	public void test() {
		// nothing yet
		assertNotNull(this.blotter.session);
		this.blotter.filter(LocalDate.now());
		await().until(() -> this.blotter.queue.size() == 3);  // pre-loaded trades for today
		this.rfq.request("Foo Ltd", CurrencyPair.currencyPair("EUR/USD"), BigDecimal.valueOf(10_000));
		await().until(() -> this.blotter.queue.size() == 4);

		await().until(() -> this.rfq.queue.size() == 3);

		this.rfq.book(((QuoteMessage) this.rfq.queue.getLast()).getQuote().getId());

		await().until(() -> {
			for (RfqMessage m: this.rfq.queue) {
				if (m instanceof QuoteMessage) {
					return ((QuoteMessage) m).getStatus() == Status.BOOKED;
				}
			}
			return false;
		});
	}

	@AfterEach
	public void after() throws IOException {
		if (this.rfq.session != null) {
			this.rfq.session.close();
		}
		if (this.blotter.session != null) {
			this.blotter.session.close();
		}
	}

	@AfterAll
	public static void afterAll() {
		Main.shutdown();
	}


	// --- Inner Classes ---

	private abstract class AbstractClient<T> extends Endpoint {
		final LinkedList<T> queue = new LinkedList<>();
		final Class<T> cls;
		Session session;

		protected AbstractClient(Class<T> cls) {
			this.cls = cls;
		}

		@Override
		public void onOpen(Session session, EndpointConfig config) {
			this.session = session;
			session.addMessageHandler(this.cls, new MessageHandler.Whole<T>() {
				@Override
				public void onMessage(T message) {
					System.out.println("Message recieved: " + message);
					queue.add(message);
				}
			});
		}

		@Override
		public void onError(Session session, Throwable thr) {
			thr.printStackTrace();
		}

		protected void send(Object msg) {
			try {
				this.session.getBasicRemote().sendObject(msg);
			} catch (IOException | EncodeException e) {
				throw new AssertionError(e);
			}
		}
	}

	private class BlotterClient extends AbstractClient<BlotterMessage> {
		BlotterClient() {
			super(BlotterMessage.class);
		}

		public void filter(LocalDate date) {
			send(new FilterMessage(format("t.timestamp >= \"%sT00:00:00.000Z\" and t.timestamp < \"%sT00:00:00.000Z\"", date, date.plusDays(1))));
		}
	}

	private class RfqClient extends AbstractClient<RfqMessage> {
		RfqClient() {
			super(RfqMessage.class);
		}

		public void request(String counterparty, CurrencyPair ccyPair,
				BigDecimal notional) {
			send(new RequestMessage(counterparty, ccyPair, notional));
		}

		public void book(int id) {
			send(new BookMessage(id));
		}
	}
}
