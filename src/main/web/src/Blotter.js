import React, { Component } from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Title from './Title';


export class Blotter extends Component {
	constructor(props) {
		super(props)
		this.state = {
			trades: { }
		}
	}

	componentDidMount = () => {
		this.worker = new Worker('BlotterWorker.js');
		this.worker.onmessage = this.onMessage;
		this.worker.onerror = this.onerror;
		this.worker.onmessageerror = this.onerror;
	}

	onMessage = e => {
		switch (e.data.type) {
			case 'OPEN': {
				this.worker.postMessage({ type: 'MSG', data: { '@type': 'FILTER', query: '' } })
				break;
			}
			case 'MSG': {
				var payload = e.data.payload;
				var trades = this.state.trades;
				switch (payload.type) {
					case 'CREATED':
					case 'UPDATED':
						trades[payload.id] = payload.trade;
						break;
					case 'REMOVED':
						delete trades[payload.id];
				}

				this.setState({ trades: trades })
				break;
			}
			case 'CLOSE':
			case 'ERROR': {
				throw e;
			}
		}
	}

	render() {
		return (
			<React.Fragment>
				<Title>Blotter</Title>
				<Table size="small">
					<TableHead>
						<TableRow>
							<TableCell>Timestamp</TableCell>
							<TableCell>Status</TableCell>
							<TableCell>Counterparty</TableCell>
							<TableCell>Currency Pair</TableCell>
							<TableCell align="right">Notional</TableCell>
							<TableCell>Buy</TableCell>
							<TableCell>Sell</TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
					{Object.keys(this.state.trades).map((item, index) => {
						var trade = this.state.trades[item]
						var quote = trade.quotes[Object.keys(trade.quotes).length]
						return ( <TableRow key={index}>
						 	<TableCell>{trade.timestamp}</TableCell>
						 	<TableCell>{trade.status}</TableCell>
						 	<TableCell>{trade.counterparty}</TableCell>
						 	<TableCell>{trade.ccyPair.substring(0,3)}/{trade.ccyPair.substring(3,6)}</TableCell>
						 	<TableCell align="right">{trade.notional.value}</TableCell>
						 	<TableCell>{quote ? quote.bid.value : null}</TableCell>
						 	<TableCell>{quote ? quote.offer.value : null}</TableCell>
						</TableRow>
					)})}
					</TableBody>
				</Table>
			</React.Fragment>
		);
	}
}

export default Blotter;
