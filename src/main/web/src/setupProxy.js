const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = function(app) {
  app.use(createProxyMiddleware(['/ws/**', '/rfq/**'], { target: 'http://localhost:7001/', ws: true }));
};
