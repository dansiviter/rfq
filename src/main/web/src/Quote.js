import React, { Component } from 'react';
import Title from './Title';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';

export class Quote extends Component {
	constructor(props) {
		super(props)
		this.state = {
			ccyPairs: [],
			counterparty: 'Acme Ltd.',
			ccyPair: 'GBPEUR',
			notional: 10000.00,
			trade: null
		}
	}

	componentDidMount = () => {
		fetch("/rfq/v1/static/currency-pairs")
		.then(res => res.json())
		.then(result => {
			this.setState({
				ccyPairs: result
			});
		  },error => {
			console.log(error)
		  }
		)

		this.worker = new Worker('RfqWorker.js');
		this.worker.onmessage = this.onMessage;
		this.worker.onerror = this.onerror;
		this.worker.onmessageerror = this.onerror;
	}

	onMessage = e => {
		switch (e.data.type) {
			case 'OPEN':
			case 'MSG':
			case 'CLOSE':
			case 'ERROR': {
				// nothing yet!
				break;
			}
			default:
				throw 'Totes nightmare!'
		}
	}

	handleCcyPair = (e) => {
		this.setState({ ccyPair: e.target.value })
	};

	handleSubmit = (e) => {
		this.worker.postMessage({ type: 'MSG', data: {
			'@type': 'REQUEST',
			counterparty: this.state.counterparty,
			ccyPair: this.state.ccyPair,
			notional: this.state.notional
		}})
		e.preventDefault();
	};

	render() {
		return (
			<React.Fragment>
				<Title>Request For Quote</Title>
				<form noValidate autoComplete="off" onSubmit={this.handleSubmit}>
					<TextField required id="standard-required" label="Counterparty" value={this.state.counterparty} />
					<TextField
						id="standard-select-currency"
						select
						label="Currency Pair"
						value={this.state.ccyPair}
						onChange={this.handleCcyPair} >
						{this.state.ccyPairs.map((option) => (
							<MenuItem key={option} value={option}>
								{option.substring(0,3)}/{option.substring(3,6)}
							</MenuItem>
						))}
					</TextField>
					<TextField
						id="outlined-number"
						label="Notional"
						type="number"
						value={this.state.notional}
						InputLabelProps={{
							shrink: true,
						}} />

					<Button type="submit">
                        Request
                    </Button>
				</form>
			</React.Fragment>
		);
	}
}

export default Quote;
