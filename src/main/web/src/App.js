import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import NotificationsIcon from '@material-ui/icons/Notifications';
import MenuIcon from '@material-ui/icons/Menu';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Link from '@material-ui/core/Link';
import Quote from './Quote';
import Blotter from './Blotter';

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://dansiviter.uk">
        Dan Siviter
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
	root: {
		display: 'flex',
  },
  title: {
    flexGrow: 1,
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: '100vh',
		overflow: 'auto',
		'& > *': {
      margin: theme.spacing(1),
    },
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
  fixedHeight: {
    height: 240,
	},
	textField: {
    width: '25ch',
  },
}));

const currencyPairs = [
  {
    value: 'GBPUSD',
    display: 'GBP/USD',
  },
  {
    value: 'GBPEUR',
    display: 'GBP/EUR',
  },
  {
    value: 'GBPAUD',
    display: 'GBP/AUD',
  },
  {
    value: 'USDAUD',
    display: 'USD/AUD',
  },
];

export default function App() {
	const classes = useStyles();

 	return (
		<React.Fragment>
			<CssBaseline />
			<div className={classes.root}>
				<AppBar position="absolute">
					<Toolbar>
						<IconButton
						edge="start"
						color="inherit">
						<MenuIcon />
						</IconButton>
							<Typography component="h1" variant="h6" color="inherit" noWrap>
								Dashboard
							</Typography>
						<IconButton color="inherit">
						<Badge badgeContent={4} color="secondary">
							<NotificationsIcon />
						</Badge>
						</IconButton>
					</Toolbar>
				</AppBar>
				<main className={classes.content}>
        	<div className={classes.appBarSpacer} />
        	<Container maxWidth="lg" className={classes.container}>
						<Grid container spacing={3}>
							<Grid item xs={12}>
								<Paper className={ classes.paper }>
									<Quote />
								</Paper>
							</Grid>
							<Grid item xs={12}>
								<Paper className={ classes.paper }>
									<Blotter />
								</Paper>
							</Grid>
						</Grid>
						<Box pt={4}>
							<Copyright />
						</Box>
					</Container>
				</main>
			</div>
		</React.Fragment>
	);
}
