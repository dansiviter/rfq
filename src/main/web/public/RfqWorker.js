importScripts('//cdn.jsdelivr.net/npm/lossless-json@1.0.3/dist/lossless-json.min.js', '//unpkg.com/uuid@latest/dist/umd/uuidv4.min.js')

var ws;

const onWsOpen = e => {
	postMessage({ type: "OPEN" });
}
const onWsMessage = e => {
	postMessage({ type: "MSG", payload: LosslessJSON.parse(e.data) })
}
const onWsClose = e => {
	postMessage({ type: "CLOSE", event: e.reason, code: e.code, wasClean: e.wasClean });
}
const onWsError = e => {
	console.log(e);
	postMessage({ type: "ERROR", event: e });
}

onmessage = e => {
	const payload = e.data.data
	switch (e.data.type) {
	case "MSG": {
		if (!this.webSocket || this.webSocket.readyState !== WebSocket.OPEN) {
			reconnect().then(e => {
				onWsOpen(e);
				ws.send(LosslessJSON.stringify(payload));
			}).catch(onWsError);
			return;
		}

		ws.send(LosslessJSON.stringify(payload));
		break;
	}
	case "RECONNECT": {
		reconnect().then(onWsOpen).catch(onWsError);
		break;
	}
	}
}

const reconnect = () => {
	if (this.ws) {
		ws.close();
	}

	return new Promise(function (resolve, reject) {
		ws = new WebSocket("ws://" + location.host + "/ws/rfq/v1/" + uuidv4());
		ws.onclose = onWsClose;
		ws.onmessage = onWsMessage;
		ws.onopen = e => {
			resolve(e);
		};
		ws.onerror = e => {
			reject(e);
		};
	});
}
