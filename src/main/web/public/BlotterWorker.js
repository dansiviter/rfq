importScripts('//cdn.jsdelivr.net/npm/lossless-json@1.0.3/dist/lossless-json.min.js')

var webSocket;

const onWsOpen = e => {
	postMessage({ type: "OPEN" });
}
const onWsMessage = e => {
	postMessage({ type: "MSG", payload: LosslessJSON.parse(e.data) })
}
const onWsClose = e => {
	postMessage({ type: "CLOSE", event: e.reason, code: e.code, wasClean: e.wasClean });
}
const onWsError = e => {
	console.log(e);
	postMessage({ type: "ERROR", event: e.toString() });
}

onmessage = e => {
	switch (e.data.type) {
	case "MSG": {
		webSocket.send(LosslessJSON.stringify(e.data.data));
		break;
	}
	case "RECONNECT": {
		reconnect();
		break;
	}
	}
}

const reconnect = () => {
	if (this.webSocket) {
		webSocket.close();
	}

	webSocket = new WebSocket("ws://" + location.host + "/ws/blotter/v1");
	webSocket.onclose = onWsClose;
	webSocket.onerror = onWsError;
	webSocket.onmessage = onWsMessage;
	webSocket.onopen = onWsOpen;
}

reconnect();
