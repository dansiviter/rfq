/*
 * Copyright 2020 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.dansiviter.rfq.api;

import static java.util.Objects.hash;
import static uk.dansiviter.rfq.Messages.messages;

import java.io.IOException;
import java.util.Currency;
import java.util.Objects;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 *
 * @author Daniel Siviter
 * @since v1.0 [22 Mar 2020]
 */
@JsonDeserialize(using = CurrencyPair.CurrencyPairDeserializer.class)
@JsonSerialize(using = CurrencyPair.CurrencyPairSerialiser.class)
public class CurrencyPair {
	private final Currency base;
	private final Currency quote;

	private CurrencyPair(Currency base, Currency quote) {
		this.base = base;
		this.quote = quote;
	}

	/**
	 * @return the base
	 */
	public Currency getBase() {
		return base;
	}

	/**
	 * @return the quote
	 */
	public Currency getQuote() {
		return quote;
	}

	@Override
	public int hashCode() {
		return hash(this.base, this.quote);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj instanceof CurrencyPair) {
			CurrencyPair other = (CurrencyPair) obj;
			return Objects.equals(this.base, other.base) && Objects.equals(this.quote, other.quote);
		}
		return false;
	}

	@JsonDeserialize
	@Override
	public String toString() {
		return this.base.getCurrencyCode().concat(this.quote.getCurrencyCode());
	}

	/**
	 *
	 * @param in
	 * @return
	 */
	public static CurrencyPair valueOf(String in) {
		return currencyPair(in);
	}

	/**
	 *
	 * @param in
	 * @return
	 */
	public static CurrencyPair currencyPair(String in) {
		if (in.length() < 6 || in.length() > 7) {
			throw messages().currencyPairLength(in.length());
		}
		String[] splits;
		if (in.contains("-") || in.contains("/")) {
			splits = in.split("-|/");
			if (splits.length > 2) {
				throw messages().currencyPairInvalidFormat(in);
			}
		} else {
			splits = new String[] { in.substring(0, 3), in.substring(3) };
		}
		for (String split : splits) {
			for (char c : split.toCharArray()) {
				if (!Character.isLetter(c)) {
					throw messages().currencyPairInvalidFormat(in);
				}
			}
		}

		return currencyPair(Currency.getInstance(splits[0]), Currency.getInstance(splits[1]));
	}

	public static CurrencyPair currencyPair(Currency base, Currency quote) {
		return new CurrencyPair(base, quote);
	}

	public static class CurrencyPairDeserializer extends JsonDeserializer<CurrencyPair> {
		@Override
		public CurrencyPair deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
			if (p.currentToken() == JsonToken.VALUE_STRING && p.getTextLength() == 6) {
				return currencyPair(p.getText());
			}
			return ctxt.readValue(p, CurrencyPair.class);
		}
	}

	public static class CurrencyPairSerialiser extends JsonSerializer<CurrencyPair> {
		@Override
		public void serialize(CurrencyPair value, JsonGenerator gen, SerializerProvider serializers)
				throws IOException {
			gen.writeObject(value.toString());
		}
	}
}
