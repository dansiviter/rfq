/*
 * Copyright 2020 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.dansiviter.rfq.api;

import java.math.BigDecimal;

import javax.annotation.Nonnull;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Daniel Siviter
 * @since v1.0 [22 Mar 2020]
 */
public class RequestMessage implements RfqMessage {
	@NotNull
	private final String counterparty;
	@NotNull
	private final CurrencyPair ccyPair;
	@NotNull
	@DecimalMin(value = "1")
	private final BigDecimal notional;

	@JsonCreator
	public RequestMessage(
		@JsonProperty("counterparty") @Nonnull String counterparty,
		@JsonProperty("ccyPair") @Nonnull CurrencyPair ccyPair,
		@JsonProperty("notional") @Nonnull BigDecimal notional)
	{
		this.counterparty = counterparty;
		this.ccyPair = ccyPair;
		this.notional = notional;
	}

	/**
	 * @return the counterparty
	 */
	public String getCounterparty() {
		return counterparty;
	}

	/**
	 * @return the ccyPair
	 */
	public CurrencyPair getCcyPair() {
		return ccyPair;
	}

	/**
	 * @return the notional
	 */
	public BigDecimal getNotional() {
		return notional;
	}
}
