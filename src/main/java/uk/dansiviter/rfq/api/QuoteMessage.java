/*
 * Copyright 2020 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.dansiviter.rfq.api;

import uk.dansiviter.rfq.entity.Quote;
import uk.dansiviter.rfq.entity.Trade.Status;

/**
 *
 * @author Daniel Siviter
 * @since v1.0 [22 Mar 2020]
 */
public class QuoteMessage implements RfqMessage {
	private final Quote quote;
	private final Status status;

	/**
	 *
	 * @param quote
	 * @param status
	 */
	public QuoteMessage(Quote quote, Status status){
		this.quote = quote;
		this.status = status;
	}

	/**
	 * @return the quote
	 */
	public Quote getQuote() {
		return quote;
	}

	/**
	 * @return the status
	 */
	public Status getStatus() {
		return status;
	}
}
