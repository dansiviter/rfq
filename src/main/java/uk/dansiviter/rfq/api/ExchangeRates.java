/*
 * Copyright 2020 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.dansiviter.rfq.api;

import static java.util.stream.Collectors.toMap;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Currency;
import java.util.Map;
import java.util.Map.Entry;

import javax.json.bind.annotation.JsonbCreator;
import javax.json.bind.annotation.JsonbProperty;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 *
 * @author Daniel Siviter
 * @since v1.0 [22 Mar 2020]
 * @see https://api.exchangeratesapi.io/
 */
public class ExchangeRates {
	private final Currency base;
	private final LocalDate date;
	private final Map<Currency, BigDecimal> rates;

	@JsonCreator
	@JsonbCreator
	public ExchangeRates(
			@JsonbProperty("rates") Map<String, BigDecimal> rates,
			@JsonbProperty("base") Currency base,
			@JsonbProperty("date") LocalDate date) {
		this.base = base;
		this.date = date;
		// eclipse-ee4j/yasson#253
		this.rates = rates.entrySet().stream().collect(toMap(e -> Currency.getInstance(e.getKey()), Entry::getValue));
	}

	/**
	 * @return the base
	 */
	public Currency getBase() {
		return base;
	}

	/**
	 * @return the date
	 */
	public LocalDate getDate() {
		return date;
	}

	/**
	 * @return the rates
	 */
	public Map<Currency, BigDecimal> getRates() {
		return rates;
	}
}
