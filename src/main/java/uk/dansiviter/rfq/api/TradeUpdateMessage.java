/*
 * Copyright 2020 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.dansiviter.rfq.api;

import java.util.UUID;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.cache.event.EventType;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import uk.dansiviter.rfq.entity.Trade;

/**
 *
 * @author Daniel Siviter
 * @since v1.0 [14 Apr 2020]
 */
public class TradeUpdateMessage implements BlotterMessage {
	private final EventType type;
	private final UUID id;
	private final Trade trade;

	@JsonCreator
	public TradeUpdateMessage(
		@JsonProperty("type") @Nonnull EventType type,
		@JsonProperty("id") @Nonnull UUID id,
		@JsonProperty("trade") @Nullable Trade trade)
	{
		this.type = type;
		this.id = id;
		this.trade = trade;
	}

	/**
	 * @return the type
	 */
	public EventType getType() {
		return type;
	}

	/**
	 * @return the id
	 */
	public UUID getId() {
		return id;
	}

	/**
	 * @return the trade
	 */
	public Trade getTrade() {
		return trade;
	}
}
