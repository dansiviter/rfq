/*
 * Copyright 2020 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.dansiviter.rfq.jsonb;

import javax.json.bind.adapter.JsonbAdapter;

import uk.dansiviter.rfq.api.CurrencyPair;

/**
 *
 * @author Daniel Siviter
 * @since v1.0 [20 May 2020]
 */
public class CurrencyPairAdapter implements JsonbAdapter<CurrencyPair, String> {
	@Override
	public String adaptToJson(CurrencyPair obj) {
		return obj.toString();
	}

	@Override
	public CurrencyPair adaptFromJson(String obj) {
		return CurrencyPair.currencyPair(obj);
	}
}
