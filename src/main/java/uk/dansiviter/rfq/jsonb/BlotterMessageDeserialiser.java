/*
 * Copyright 2020 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.dansiviter.rfq.jsonb;

import java.lang.reflect.Type;

import javax.json.bind.serializer.DeserializationContext;
import javax.json.bind.serializer.JsonbDeserializer;
import javax.json.bind.serializer.JsonbSerializer;
import javax.json.bind.serializer.SerializationContext;
import javax.json.stream.JsonGenerator;
import javax.json.stream.JsonParser;

import uk.dansiviter.rfq.api.BlotterMessage;

/**
 *
 * @author Daniel Siviter
 * @since v1.0 [20 May 2020]
 */
public class BlotterMessageDeserialiser implements JsonbDeserializer<BlotterMessage>, JsonbSerializer<BlotterMessage> {
	@Override
	public BlotterMessage deserialize(JsonParser parser, DeserializationContext ctx, Type rtType) {
		BlotterMessage msg = null;
        while (parser.hasNext()) {
            JsonParser.Event event = parser.next();
            if (event == JsonParser.Event.KEY_NAME) {
                String className = parser.getString();
                parser.next();
                try {
                    msg = ctx.deserialize(Class.forName(className).asSubclass(BlotterMessage.class), parser);
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
        return msg;
	}

	@Override
	public void serialize(BlotterMessage msg, JsonGenerator jsonGenerator, SerializationContext serializationContext) {
        if (msg != null) {
            serializationContext.serialize(msg.getClass().getName(), msg, jsonGenerator);
        } else {
            serializationContext.serialize(null, jsonGenerator);
        }
    }
}
