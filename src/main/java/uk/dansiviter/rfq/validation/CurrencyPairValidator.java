/*
 * Copyright 2020 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.dansiviter.rfq.validation;

import javax.enterprise.inject.spi.CDI;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import uk.dansiviter.rfq.api.CurrencyPair;
import uk.dansiviter.rfq.service.CurrencyPairService;

public class CurrencyPairValidator implements ConstraintValidator<ValidCurrencyPair, CurrencyPair> {
	private CurrencyPairService svc;

	@Override
    public void initialize(ValidCurrencyPair ageValue) {
        this.svc = CDI.current().select(CurrencyPairService.class).get();
	}

    @Override
    public boolean isValid(CurrencyPair ccyPair, ConstraintValidatorContext constraintValidatorContext) {
		return this.svc.getCurrencyPairs().contains(ccyPair);
    }
}
