/*
 * Copyright 2020 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.dansiviter.rfq;

import java.io.IOException;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;

import org.infinispan.cdi.embedded.ConfigureCache;
import org.infinispan.configuration.cache.Configuration;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.configuration.global.GlobalConfigurationBuilder;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;

import uk.dansiviter.rfq.annotations.TradeCache;
import uk.dansiviter.rfq.entity.Trade;

/**
 *
 * @author Daniel Siviter
 * @since v1.0 [14 Apr 2020]
 */
@ApplicationScoped
public class CacheProducer {
	@Produces
	@ApplicationScoped
	public EmbeddedCacheManager defaultClusteredCacheManager() {
		return new DefaultCacheManager(new GlobalConfigurationBuilder().defaultCacheName("default").build(),
				new ConfigurationBuilder().memory().size(7).build());
	}

	@ConfigureCache("trade-cache") // This is the cache name.
	@TradeCache // This is the cache qualifier.
	@Produces
	public Configuration tradeCache() {
		return new ConfigurationBuilder().indexing().addIndexedEntities(Trade.class).build();
	}

	public static void destroy(@Disposes EmbeddedCacheManager cacheManager) {
		try {
			cacheManager.close();
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}
}
