/*
 * Copyright 2020 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.dansiviter.rfq.ws;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

import javax.enterprise.inject.spi.CDI;
import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

import com.fasterxml.jackson.databind.ObjectMapper;

import uk.dansiviter.rfq.api.RfqMessage;

/**
 *
 * @author Daniel Siviter
 * @since v1.0 [22 Mar 2020]
 */
public class RfqMessageCodec implements Encoder.TextStream<RfqMessage>, Decoder.TextStream<RfqMessage> {
	private ObjectMapper mapper;

	@Override
	public void init(EndpointConfig config) {
		this.mapper = CDI.current().select(ObjectMapper.class).get();
	}

	@Override
	public RfqMessage decode(Reader reader) throws DecodeException, IOException {
		return this.mapper.readValue(reader, RfqMessage.class);
	}

	@Override
	public void encode(RfqMessage object, Writer writer) throws EncodeException, IOException {
		this.mapper.writeValue(writer, object);
	}

	@Override
	public void destroy() {
		// NOOP
	}
}
