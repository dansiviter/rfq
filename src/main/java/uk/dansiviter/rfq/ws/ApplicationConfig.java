package uk.dansiviter.rfq.ws;

import java.util.Collections;
import java.util.Set;

import javax.enterprise.context.ApplicationScoped;
import javax.websocket.Endpoint;
import javax.websocket.server.ServerApplicationConfig;
import javax.websocket.server.ServerEndpointConfig;

import io.helidon.microprofile.server.RoutingPath;

/**
 *
 * @author Daniel Siviter
 * @since v1.0 [3 Apr 2020]
 */
@ApplicationScoped
@RoutingPath("ws")
public class ApplicationConfig implements ServerApplicationConfig {
	@Override
	public Set<ServerEndpointConfig> getEndpointConfigs(Set<Class<? extends Endpoint>> endpointClasses) {
		return Collections.emptySet();
	}

	@Override
	public Set<Class<?>> getAnnotatedEndpointClasses(Set<Class<?>> scanned) {
		return scanned;
	}
}
