/*
 * Copyright 2020 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.dansiviter.rfq.ws;

import java.io.Closeable;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.BeanManager;
import javax.inject.Inject;
import javax.websocket.Session;

import org.tomitribe.microscoped.core.ScopeContext;

import uk.dansiviter.rfq.Log;

/**
 * Holds a WebSocket {@link Session} within the CDI runtime.
 *
 * @author Daniel Siviter
 * @since v1.0 [17 Aug 2016]
 */
@ApplicationScoped
public class SessionHolder {
	private static final ThreadLocal<Session> SESSION = new ThreadLocal<>();

	@Inject
	private Log log;
	@Inject
	private BeanManager beanManager;

	/**
	 *
	 * @param session the session to set on the thread local context.
	 * @return the closable to reset the instance.
	 */
	public Scope set(Session session) {
		this.log.debugf("Setting session. [%s]", session.getId());
		final ScopeContext<Session> scopeCtx = webSocketContext();

		final Session previous = scopeCtx.enter(session);
		SESSION.set(session);
		return () -> {
			scopeCtx.exit(previous);
			SESSION.remove();
		};
	}

	/**
	 * @param context the current context.
	 * @return the current WebSocket Session.
	 */
	@Produces
	public Session get() {
		return SESSION.get();
	}

	/**
	 * @param beanManager the bean manager instance.
	 * @return the WebSocket {@link ScopeContext}.
	 */
	@SuppressWarnings("unchecked")
	public ScopeContext<Session> webSocketContext() {
		return (ScopeContext<Session>) beanManager.getContext(WebSocketScope.class);
	}

	public static interface Scope extends Closeable {
		@Override
		void close();
	}
}
