/*
 * Copyright 2020 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.dansiviter.rfq.ws;

import javax.inject.Inject;
import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.PongMessage;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import uk.dansiviter.rfq.Log;
import uk.dansiviter.rfq.api.BlotterMessage;
import uk.dansiviter.rfq.api.FilterMessage;
import uk.dansiviter.rfq.service.BlotterService;
import uk.dansiviter.rfq.ws.SessionHolder.Scope;

/**
 *
 * @author Daniel Siviter
 * @since v1.0 [13 Apr 2020]
 */
@ServerEndpoint(
	value = "/blotter/v1",
	decoders = BlotterMessageCodec.class,
	encoders = BlotterMessageCodec.class,
	subprotocols = "blotter")
public class BlotterEndpoint {
	@Inject
	private Log log;
	@Inject
	private SessionHolder sessionHolder;
	@Inject
	public BlotterService service;

	@OnOpen
	public void onOpen(Session session) {
		try (Scope scope = sessionHolder.set(session)) {
			this.log.onOpen(session.getId());
			// do something!
		}
	}

	@OnMessage
	public void onMessage(Session session, BlotterMessage msg) {
		try (Scope scope = sessionHolder.set(session)) {
			if (msg instanceof FilterMessage) {
				service.start((FilterMessage) msg);
				return;
			}
			throw new IllegalStateException("Unknown message type! [" + msg + "]");
		}
	}

	@OnMessage
	public void onMessage(Session session, PongMessage msg) {
		try (Scope scope = sessionHolder.set(session)) {
			// do something
		}
	}

	@OnClose
	public void onClose(Session session, CloseReason reason) {
		try (Scope scope = sessionHolder.set(session)) {
			this.log.onClose(session.getId(), reason.getCloseCode().getCode(), reason.getReasonPhrase());
		} finally {
			this.sessionHolder.webSocketContext().destroy(session);
		}
	}

	@OnError
	public void onError(Session session, Throwable throwable) {
		try (Scope scope = sessionHolder.set(session)) {
			this.log.onError(session.getId(), throwable);
		}
	}
}
