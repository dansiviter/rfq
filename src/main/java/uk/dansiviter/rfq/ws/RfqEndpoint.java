/*
 * Copyright 2020 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.dansiviter.rfq.ws;

import java.util.UUID;

import javax.inject.Inject;
import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

import uk.dansiviter.rfq.Log;
import uk.dansiviter.rfq.api.BookMessage;
import uk.dansiviter.rfq.api.RequestMessage;
import uk.dansiviter.rfq.api.RfqMessage;
import uk.dansiviter.rfq.service.QuoteService;
import uk.dansiviter.rfq.ws.SessionHolder.Scope;

/**
 *
 * @author Daniel Siviter
 * @since v1.0 [22 Mar 2020]
 */
@ServerEndpoint(
	value = "/rfq/v1/{id}",
	decoders = RfqMessageCodec.class,
	encoders = RfqMessageCodec.class,
	subprotocols = "rfq")
public class RfqEndpoint {
	@Inject
	private Log log;
	@Inject
	private SessionHolder sessionHolder;
	@Inject
	private QuoteService quoteService;

	@OnOpen
	public void onOpen(Session session, @PathParam("id") String id) {
		try (Scope scope = sessionHolder.set(session)) {
			this.log.onOpen(session.getId());
			quoteService.validate(UUID.fromString(id));
		}
	}

	@OnMessage
	public void onMessage(Session session, @PathParam("id") String id, RfqMessage msg) {
		try (Scope scope = sessionHolder.set(session)) {
			if (msg instanceof RequestMessage) {
				quoteService.start(UUID.fromString(id), (RequestMessage) msg);
				return;
			}
			if (msg instanceof BookMessage) {
				quoteService.book(UUID.fromString(id), ((BookMessage) msg).getId());
				return;
			}
			throw new IllegalStateException("Unknown message type! [" + msg + "]");
		}
	}

	@OnClose
	public void onClose(Session session, @PathParam("id") String id, CloseReason reason) {
		try (Scope scope = sessionHolder.set(session)) {
			this.log.onClose(session.getId(), reason.getCloseCode().getCode(), reason.getReasonPhrase());
		} finally {
			this.sessionHolder.webSocketContext().destroy(session);
		}
	}

	@OnError
	public void onError(Session session, @PathParam("id") String id, Throwable throwable) {
		try (Scope scope = sessionHolder.set(session)) {
			this.log.onError(session.getId(), throwable);
		}
	}
}
