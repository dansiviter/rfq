/*
 * Copyright 2020 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.dansiviter.rfq.entity;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import uk.dansiviter.rfq.api.CurrencyPair;
import uk.dansiviter.rfq.api.RequestMessage;

/**
 *
 * @author Daniel Siviter
 * @since v1.0 [14 Apr 2020]
 */
@JsonDeserialize(builder = Trade.Builder.class)
public class Trade {
	private final Instant timestamp;
	private final String counterparty;
	private final CurrencyPair ccyPair;
	private final Map<Integer, Quote> quotes;
	private final BigDecimal notional;
	private final Status status;

	public Trade(Builder builder) {
		this.timestamp = builder.timestamp == null ? Instant.now() : builder.timestamp;
		this.counterparty = builder.counterparty;
		this.ccyPair = builder.ccyPair;
		this.notional = builder.notional;
		this.quotes = Map.copyOf(builder.quotes);
		this.status = builder.status;
	}

	/**
	 * @return the timestamp
	 */
	public Instant getTimestamp() {
		return timestamp;
	}

	/**
	 * @return the counterparty
	 */
	public String getCounterparty() {
		return counterparty;
	}

	/**
	 * @return the ccyPair
	 */
	public CurrencyPair getCcyPair() {
		return ccyPair;
	}

	/**
	 * @return the notional
	 */
	public BigDecimal getNotional() {
		return notional;
	}

	/**
	 * @return the quotes
	 */
	public Map<Integer, Quote> getQuotes() {
		return quotes;
	}

	/**
	 * @return the status
	 */
	public Status getStatus() {
		return status;
	}

	public static Builder tradeBuilder() {
		return new Builder();
	}

	public enum Status {
		REQUESTED,
		QUOTE,
		BOOKED,
		REJECTED,
		EXPIRED,
		CANCELLED
	}

	@JsonPOJOBuilder(buildMethodName = "build", withPrefix = "")
	public static class Builder {
		private Instant timestamp;
		private String counterparty;
		private CurrencyPair ccyPair;
		private BigDecimal notional;
		private final Map<Integer, Quote> quotes = new HashMap<>();
		private Status status;

		public Builder timestamp(Instant timestamp) {
			this.timestamp = timestamp;
			return this;
		}

		public Builder counterparty(String counterparty) {
			this.counterparty = counterparty;
			return this;
		}

		public Builder ccyPair(CurrencyPair ccyPair) {
			this.ccyPair = ccyPair;
			return this;
		}

		public Builder notional(BigDecimal notional) {
			this.notional = notional;
			return this;
		}

		public Builder quotes(Map<Integer, Quote> quotes) {
			this.quotes.clear();
			this.quotes.putAll(quotes);
			return this;
		}

		public Builder quote(Quote quote) {
			this.quotes.put(quote.getId(), quote);
			return this;
		}

		public Builder status(Status status) {
			this.status = status;
			return this;
		}

		public Builder requestMessage(RequestMessage msg) {
			this.counterparty = msg.getCounterparty();
			this.ccyPair = msg.getCcyPair();
			this.notional = msg.getNotional();
			this.status = Status.REQUESTED;
			return this;
		}

		public Builder trade(Trade trade) {
			timestamp(trade.timestamp);
			counterparty(trade.counterparty);
			ccyPair(trade.ccyPair);
			notional(trade.notional);
			quotes(trade.quotes);
			status(trade.status);
			return this;
		}

		public Trade build() {
			return new Trade(this);
		}
	}
}
