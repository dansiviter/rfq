/*
 * Copyright 2020 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.dansiviter.rfq.entity;

import java.math.BigDecimal;
import java.time.Instant;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Quote {
	private final int id;
	private final BigDecimal bid;
	private final BigDecimal offer;
	private final Instant timestamp;

	@JsonCreator
	public Quote(
			@JsonProperty("quoteId") int id,
			@JsonProperty("bid")  BigDecimal bid,
			@JsonProperty("offer") BigDecimal offer,
			@JsonProperty("timestamp") Instant timestamp)
	{
		this.id = id;
		this.bid = bid;
		this.offer = offer;
		this.timestamp = timestamp;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return the bid
	 */
	public BigDecimal getBid() {
		return bid;
	}

	/**
	 * @return the offer
	 */
	public BigDecimal getOffer() {
		return offer;
	}

	/**
	 * @return the timestamp
	 */
	public Instant getTimestamp() {
		return timestamp;
	}
}
