/*
 * Copyright 2020 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.dansiviter.rfq.rest;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;

import uk.dansiviter.rfq.api.CurrencyPair;
import uk.dansiviter.rfq.service.CurrencyPairService;

@Path("static")
@Produces(APPLICATION_JSON)
@Singleton
public class StaticDataResource {
	@Inject
	private CurrencyPairService currencyPairService;

	@GET
	@Path("currency-pairs")
	@APIResponse(responseCode = "200", description = "List of Currency Pairs")
	public List<CurrencyPair> currencyPairs() {
		return this.currencyPairService.getCurrencyPairs();
	}
}
