/*
 * Copyright 2020 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.dansiviter.rfq;

import org.jboss.logging.annotations.Message;
import org.jboss.logging.annotations.MessageBundle;

@MessageBundle(projectCode = "RFQ")
public interface Messages {
	Messages INSTANCE = org.jboss.logging.Messages.getBundle(Messages.class);

	@Message(value = "Currency pair should be >=6 or <=7! [%d]")
	IllegalArgumentException currencyPairLength(int length);

	@Message(value = "Currency pair is in invalid format! It should only contain alphabetic characters and optionally a hyphen or forward slash. [%s]")
	IllegalArgumentException currencyPairInvalidFormat(String ccyPair);

	public static Messages messages() {
		return INSTANCE;
	}
}
