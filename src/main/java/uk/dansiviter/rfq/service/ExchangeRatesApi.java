/*
 * Copyright 2020 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.dansiviter.rfq.service;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.time.LocalDate;
import java.util.Currency;
import java.util.List;
import java.util.concurrent.CompletionStage;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.ext.ParamConverter;
import javax.ws.rs.ext.ParamConverterProvider;
import javax.ws.rs.ext.Provider;

import org.eclipse.microprofile.rest.client.annotation.RegisterProvider;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import uk.dansiviter.rfq.api.ExchangeRates;
import uk.dansiviter.rfq.rest.JsonbContextResolver;

/**
 *
 * @author Daniel Siviter
 * @since v1.0 [22 Mar 2020]
 * @see https://api.exchangeratesapi.io/
 */
@RegisterRestClient(baseUri = "https://api.exchangeratesapi.io/")
@RegisterProvider(ExchangeRatesApi.ParamConverterProviderImpl.class)
@RegisterProvider(JsonbContextResolver.class)
@Consumes(APPLICATION_JSON)
public interface ExchangeRatesApi {
	/**
	 *
	 * @param base
	 * @return
	 */
	@GET
	@Path("latest")
	CompletionStage<ExchangeRates> latest(@QueryParam("base") Currency base);

	/**
	 *
	 * @param base
	 * @param symbols
	 * @return
	 */
	@GET
	@Path("latest")
	CompletionStage<ExchangeRates> latest(@QueryParam("base") Currency base,
			@QueryParam("symbols") List<Currency> symbols);

	/**
	 *
	 * @param date
	 * @param base
	 * @return
	 */
	@GET
	@Path("{date}")
	CompletionStage<ExchangeRates> historical(@PathParam("date") @NotNull LocalDate date,
			@QueryParam("base") Currency base);

	/**
	 *
	 * @param date
	 * @param base
	 * @return
	 */
	@GET
	@Path("history")
	CompletionStage<ExchangeRates> historical(@QueryParam("base") Currency base,
			@QueryParam("start_at") @NotNull LocalDate start, @QueryParam("end_at") @NotNull LocalDate end,
			@QueryParam("symbols") List<Currency> symbols);

	@Provider
	public static class ParamConverterProviderImpl implements ParamConverterProvider {
		@Override
		@SuppressWarnings("unchecked")
		public <T> ParamConverter<T> getConverter(Class<T> rawType, Type genericType, Annotation[] annotations) {
			if (rawType == Currency.class) {
				return (ParamConverter<T>) new CurrencyParamConverter();
			}
			return null;
		}

		private static class CurrencyParamConverter implements ParamConverter<Currency> {
			@Override
			public Currency fromString(String value) {
				return Currency.getInstance(value);
			}

			@Override
			public String toString(Currency value) {
				return value.getCurrencyCode();
			}
		}
	}
}
