/*
 * Copyright 2020 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.dansiviter.rfq.service;

import static java.math.RoundingMode.HALF_UP;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.UUID;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nonnull;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.websocket.EncodeException;
import javax.websocket.Session;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.infinispan.Cache;

import uk.dansiviter.rfq.Log;
import uk.dansiviter.rfq.annotations.TradeCache;
import uk.dansiviter.rfq.api.QuoteMessage;
import uk.dansiviter.rfq.api.RequestMessage;
import uk.dansiviter.rfq.entity.Quote;
import uk.dansiviter.rfq.entity.Trade;
import uk.dansiviter.rfq.entity.Trade.Status;
import uk.dansiviter.rfq.ws.WebSocketScope;

/**
 *
 * @author Daniel Siviter
 * @since v1.0 [22 Mar 2020]
 */
@WebSocketScope
public class QuoteService {
	@Inject
	private Log log;
	@Inject
	private Session session;
	@Inject
	private ScheduledExecutorService executor;
	@Inject
	@RestClient
	private ExchangeRatesApi rates;
	@Inject
	@TradeCache
	private Cache<UUID, Trade> trades;

	private UUID id;
	private RequestMessage rfq;
	private ScheduledFuture<?> future;

	/**
	 *
	 * @param id
	 * @return {@code true} if it is a valid new ID.
	 */
	public boolean validate(UUID id) {
		return !trades.containsKey(id);
	}

	public void start(@Nonnull UUID id, @Valid RequestMessage rfq) {
		trades.put(id, Trade.tradeBuilder().requestMessage(rfq).build());
		this.id = id;
		this.rfq = rfq;
		this.future = this.executor.scheduleAtFixedRate(this::run, 0, 1, TimeUnit.SECONDS);
	}

	public void book(@Nonnull UUID id, @Min(1) int quoteId) {
		stop();
		Trade trade = this.trades.get(id);
		Quote quote = trade.getQuotes().get(quoteId);
		this.trades.put(id, Trade.tradeBuilder().trade(trade).status(Status.BOOKED).build());
		try {
			session.getBasicRemote().sendObject(new QuoteMessage(quote, Status.BOOKED));
		} catch (IOException | EncodeException e) {
			log.warn("Unable to book!", e);
		}
	}

	@PreDestroy
	public void destroy() {
		if (this.future != null && !this.future.isDone()) {
			this.trades.put(id, Trade.tradeBuilder().trade(this.trades.get(id)).status(Status.CANCELLED).build());
		}
		stop();
	}

	private void stop() {
		if (future != null) {
			future.cancel(false);
		}
	}

	private static BigDecimal random(double min, double max) {
		return BigDecimal.valueOf(min + (max - min) * Math.random()).setScale(3, HALF_UP);
	}

	private static BigDecimal random() {
		return random(-0.1, 0.1);
	}

	private void run() {
		final Trade trade = trades.get(this.id);
		try {
			if (trade.getQuotes().isEmpty()) {
				rates.latest(rfq.getCcyPair().getBase()/* , rfq.getCcyPair().getQuote() */).thenAccept(r -> {
					BigDecimal mid = r.getRates().get(rfq.getCcyPair().getQuote()).setScale(3, HALF_UP);
					BigDecimal delta = random();
					Quote quote = new Quote(1, mid.subtract(delta), mid.add(delta), Instant.now());
					trades.put(this.id, Trade.tradeBuilder().trade(trade).quote(quote).build());
					run();
				}).exceptionally(ex -> {
					log.error("Unable to get rates!", ex);
					return null;
				}).toCompletableFuture().join();
				return;
			}
			if (trade.getQuotes().size() >= 10) {
				trades.put(this.id, Trade.tradeBuilder().trade(trade).status(Status.EXPIRED).build());
				session.getBasicRemote().sendObject(new QuoteMessage(null, Status.EXPIRED));
				stop();
				return;
			}

			BigDecimal random = random();
			Quote prevQuote = trade.getQuotes().get(trade.getQuotes().size());
			Quote newQuote = new Quote(prevQuote.getId() + 1, prevQuote.getBid().add(random),
					prevQuote.getOffer().add(random), Instant.now());

			trades.put(this.id, Trade.tradeBuilder().trade(trade).quote(newQuote).status(Status.QUOTE).build());
			session.getBasicRemote().sendObject(new QuoteMessage(newQuote, Status.QUOTE));
		} catch (IOException | EncodeException e) {
			log.warn("Unable to send!", e);
		} catch (RuntimeException e) {
			log.warn("Unable to process!", e);
		}
	}
}
