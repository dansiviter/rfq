/*
 * Copyright 2020 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.dansiviter.rfq.service;

import static java.lang.String.format;

import java.io.IOException;
import java.util.UUID;

import javax.annotation.Nullable;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.cache.event.EventType;
import javax.inject.Inject;
import javax.websocket.EncodeException;
import javax.websocket.Session;

import org.infinispan.Cache;
import org.infinispan.query.Search;
import org.infinispan.query.api.continuous.ContinuousQuery;
import org.infinispan.query.api.continuous.ContinuousQueryListener;
import org.infinispan.query.dsl.Query;
import org.infinispan.query.dsl.QueryFactory;

import io.reactivex.annotations.NonNull;
import uk.dansiviter.rfq.Log;
import uk.dansiviter.rfq.annotations.TradeCache;
import uk.dansiviter.rfq.api.FilterMessage;
import uk.dansiviter.rfq.api.TradeUpdateMessage;
import uk.dansiviter.rfq.entity.Trade;
import uk.dansiviter.rfq.ws.WebSocketScope;

/**
 *
 * @author Daniel Siviter
 * @since v1.0 [13 Apr 2020]
 */
@WebSocketScope
public class BlotterService implements ContinuousQueryListener<UUID, Trade> {
	@Inject
	private Log log;
	@Inject
	@TradeCache
	private Cache<UUID, Trade> trades;
	@Inject
	private Session session;

	private ContinuousQuery<UUID, Trade> continuousQuery;

	@PostConstruct
	public void init() {
		this.continuousQuery = Search.getContinuousQuery(this.trades);
	}

	public void start(FilterMessage filter) {
		destroy();
		QueryFactory queryFactory = Search.getQueryFactory(this.trades);
		Query query;
		if (filter.getQuery() == null || filter.getQuery().isEmpty()) {
			query = queryFactory.create("from uk.dansiviter.rfq.entity.Trade t");
		} else {
			query = queryFactory.create(format("from uk.dansiviter.rfq.entity.Trade t where %s", filter.getQuery()));
		}
		continuousQuery.addContinuousQueryListener(query, this);
	}

	@PreDestroy
	public void destroy() {
		continuousQuery.removeContinuousQueryListener(this);
	}

	@Override
	public void resultJoining(UUID key, Trade value) {
		send(EventType.CREATED, key, value);
	}

	@Override
	public void resultUpdated(UUID key, Trade value) {
		send(EventType.UPDATED, key, value);
	}

	@Override
	public void resultLeaving(UUID key) {
		send(EventType.REMOVED, key, null);
	}

	private void send(@NonNull EventType type, @NonNull UUID id, @Nullable Trade trade) {
		try {
			this.session.getBasicRemote().sendObject(new TradeUpdateMessage(type, id, trade));
		} catch (IOException | EncodeException e) {
			this.log.warn("Unable to send!", e);
		}
	}
}
