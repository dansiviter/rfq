/*
 * Copyright 2020 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.dansiviter.rfq.service;

import static uk.dansiviter.rfq.api.CurrencyPair.currencyPair;
import static uk.dansiviter.rfq.entity.Trade.tradeBuilder;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.OffsetTime;
import java.util.Map;
import java.util.UUID;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Initialized;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.infinispan.Cache;

import uk.dansiviter.rfq.annotations.TradeCache;
import uk.dansiviter.rfq.entity.Quote;
import uk.dansiviter.rfq.entity.Trade;
import uk.dansiviter.rfq.entity.Trade.Status;

/**
 *
 * @author Daniel Siviter
 * @since v1.0 [15 Apr 2020]
 */
@ApplicationScoped
public class ExampleDataService {
	@Inject
	@TradeCache
	private Cache<UUID, Trade> trades;

	public void init(@Observes @Initialized(ApplicationScoped.class) Object init) {
		trades.putAll(createDefault());
	}

	private static Map<UUID, Trade> createDefault() {
		LocalDate today = LocalDate.now();
		return Map.of(
			UUID.randomUUID(),
			tradeBuilder()
					.timestamp(today.atTime(OffsetTime.parse("12:36:22.123Z")).toInstant())
					.counterparty("Foo Corp.")
					.ccyPair(currencyPair("EURUSD"))
					.notional(new BigDecimal("1500000.00"))
					.quote(new Quote(1, new BigDecimal("1.63"), new BigDecimal("1.52"), null))
					.status(Status.BOOKED)
					.build(),
			UUID.randomUUID(),
			tradeBuilder()
					.timestamp(today.atTime(OffsetTime.parse("16:36:22.123Z")).toInstant())
					.counterparty("Acme Ltd.")
					.ccyPair(currencyPair("GBPUSD"))
					.notional(new BigDecimal("1500.00"))
					.quote(new Quote(1, new BigDecimal("1.40"), new BigDecimal("1.30"), null))
					.status(Status.EXPIRED)
					.build(),
			UUID.randomUUID(),
			tradeBuilder()
					.timestamp(today.atTime(OffsetTime.parse("07:38:22.123Z")).toInstant())
					.counterparty("Foo Corp.")
					.ccyPair(currencyPair("AUDGBP"))
					.notional(new BigDecimal("220500.00"))
					.quote(new Quote(1, new BigDecimal("2.45"), new BigDecimal("2.30"), null))
					.status(Status.BOOKED)
					.build(),
			UUID.randomUUID(),
			tradeBuilder()
					.timestamp(today.minusDays(1).atTime(OffsetTime.parse("07:38:22.123Z")).toInstant())
					.counterparty("Acme Ltd.")
					.ccyPair(currencyPair("AUDGBP"))
					.notional(new BigDecimal("32500000.00"))
					.quote(new Quote(1, new BigDecimal("1.20"), new BigDecimal("1.05"), null))
					.status(Status.BOOKED)
					.build());
	}
}
