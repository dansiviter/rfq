/*
 * Copyright 2020 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.dansiviter.rfq.service;

import static java.util.stream.Collectors.toList;
import static uk.dansiviter.rfq.api.CurrencyPair.currencyPair;

import java.util.Currency;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.eclipse.microprofile.rest.client.inject.RestClient;

import uk.dansiviter.rfq.api.CurrencyPair;
import uk.dansiviter.rfq.api.ExchangeRates;

@ApplicationScoped
public class CurrencyPairService {
	private List<CurrencyPair> currencyPairs;

	@Inject
	@RestClient
	private ExchangeRatesApi rates;

	@PostConstruct
	public void init() {
		this.rates.latest(Currency.getInstance("GBP")).thenAccept(this::store).toCompletableFuture().join();
	}

	private void store(ExchangeRates rates) {
		List<CurrencyPair> pairs = rates.getRates()
				.keySet()
				.stream()
				.map(q -> currencyPair(rates.getBase(), q))
				.collect(toList());
		this.currencyPairs = List.copyOf(pairs);
	}

	/**
	 * @return the currencyPairs
	 */
	public List<CurrencyPair> getCurrencyPairs() {
		return currencyPairs;
	}
}
