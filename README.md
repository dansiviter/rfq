# Request For Quote PoC #

This is a small app to validate Helidon WebSocket as an RFQ server.

## Build Locally ##

Via Maven:

```
mvn clean install
```

Via Docker:
```
docker build -t rfq .
```


## Specification ##

### ReST API ###

#### Static Data ###

**Currency Pairs**

* Path: `/rfq/v1/static/currency-pairs`
* Method: `GET`
* Accept: `application/json`
* Response:

```
OK 200
content-type: application/json

[
  "GBPUSD",
  "GBPEUR",
  ...
]
```

### WebSocket API ###

#### RFQ ####

* Path: `/rfq/v1/ws/{id}`
* Path Params:
  - `id`: UUID of new RFQ
* Subprotocol: `rfq`
* Content-type: `application/json`

##### Messages #####

All messages will have a top level `@type` element which defines the type of message.

**Request for Quote**

Client request for quotes:
```
{
	"@type": "rfq",
	"ccyPair: "GBPEUR",
	"notional": 10000.00
}
```

**Quote**

Server response of quotes:
```
{
    "@type": "quote",
    "id": 12345,  # incremental quote identifier
    "bid": 1.0030,
    "offer": 1.0041
}
```

**Book**

Client request to book quote:
```
{
    "@type": "quote",
    "quoteId": 12345  # the ID of the quote to book
}
```

**Book Response**

Server response to book quote:
```
{
    "@type": "bookResponse",
    "quoteId": 12345,  # the ID of the quote to book
    "success": true
  [ "msg": "Error occured" ]  # Optional message, generally booking failures
}
```
